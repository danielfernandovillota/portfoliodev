/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {

      fontFamily:{
        roboto:['Roboto', 'sans-serif'],
      },

      colors: {
        'app-primary': {
          '50': '#fff0f0',
          '100': '#ffdddd',
          '200': '#ffc0c0',
          '300': '#ff9494',
          '400': '#ff5757',
          '500': '#ff2323',
          '600': '#ff0000',
          '700': '#d70000',
          '800': '#b10303',
          '900': '#920a0a',
          '950': '#500000',
      }, 
      'app-secundary': {
        '50': '#F7F7F7',
        '100': '#E3E3E3',
        '200': '#C8C8C8',
        '300': '#A4A4A4',
        '400': '#818181',
        '500': '#666666',
        '600': '#515151',
        '700': '#434343',
        '800': '#383838',
        '900': '##313131',
        '950': '#1C1C1C',
    },      
    },
    
    },
  },
  plugins: [],
}

