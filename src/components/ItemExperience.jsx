import React from 'react'

export const ItemExperience = ({data}) => {
  return (
    
    <aside className='mt-4 flex flex-col items-center gap-x-6 lg:flex-row'>
    
    <img src={data.icon} className='h-10 w-10'/>
      <section className='px-12  lg:text-left'>
        <h2 className=' font-extrabold text-lg uppercase text-app-primary-900'>{data.title}</h2>
        <p className={`mt-3 text-justify text-sm text-app-secundary-950 font-semibold`}>
          {data.subTitle}
        </p>
      </section>
    </aside>
  )
}
export default ItemExperience
