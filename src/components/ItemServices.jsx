import React from 'react'

export const ItemServices = ({data}) => {
    const orientationLeft = data.orientation === 'left';
    const reverse = orientationLeft ? 'flex-row' : 'flex-row-reverse';
    const orientationText = orientationLeft ? 'text-left' : 'text-right';
    
  return (
    <aside className={`flex flex-col items-center gap-x-8 lg:w-[700px] lg:${reverse}`}>
      <img src={data.icon} alt={data.title} className='h-40 w-40' />
      <section className={`text-justify lg:${orientationText} text-app-primary-900 font-extrabold text-lg uppercase`}>
        <h2 className='text-center font-extrabold text-lg'>{data.title}</h2>
        <p className={`mt-3 text-sm text-app-secundary-950 font-semibold`}>
          {data.subTitle}
        </p>
      </section>
    </aside>
  )
}
export default ItemServices
