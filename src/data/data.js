import logo from './../assets/images/logo1.png';
import img from './../assets/images/img.svg'
import daniel from './../assets/images/daniel.jpeg'
import numero1 from './../assets/images/numero1.png'
import numero2 from './../assets/images/numero2.png'
import numero3 from './../assets/images/numero3.png'
import numero4 from './../assets/images/numero4.png'
import gitlab from './../assets/images/gitlab.svg'
import instagram from './../assets/images/instagram.svg';
import futbol from './../assets/images/futbol.jpg'
import anime from './../assets/images/anime.jpg'
import dibujo from './../assets/images/dibujo.jpg'
import spagueti from './../assets/images/spagueti.jpg'
import videojugo from './../assets/images/videojuego.jpg'
import viaje from './../assets/images/viaje.jpg'

export const HeaderData = {
    logo: {
        title: 'UCC',
        link: 'index.html',
        icon: logo,
      },
      nav: [
        {
          id: 1,
          title: 'Inicio',
          link: '#inicio',
        },
        {
          id: 2,
          title: 'Servicios',
          link: '#servicios',
        },
        {
          id: 3,
          title: 'Experiencia',
          link: '#experiencia',
        },
        {
          id: 4,
          title: 'Portafolio',
          link: '#portafolio',
        },
        {
          id: 5,
          title: 'Hobbies',
          link: '#hobbies',
        },
        {
          id: 6,
          title: 'Contácto',
          link: '#contacto',
        },
      ],
      social: [
        {
          id: 1,
          title: 'GitLab',
          link: 'https://gitlab.com/dashboard/projects',
          icon: gitlab,
        },
        {
          id: 2,
          title: 'Instagram',
          link: 'https://www.instagram.com/',
          icon: instagram,
        },
      ],
};

export const HeroData = {
  title: 'Hola !\n Soy Daniel',
  subTitle:
    'Soy un Junior Developer, nacido en Palmira, colombia. En mi tiempo libre practico desarrollo en la construccion de proyectos y amante al deporte. Te invito a descargar mi CV y a navegar en mi portafolio web.',
  btnTextDownloadCv: 'Descargar CV',
  btnTextContact: 'Contáctame',
  iconHero: daniel,
  linkUno: '#contacto'
};

export const ServiceData = {
  header: {
    title: 'Servicios',
    subTitle:
      'Cada uno de nuestros servicios tiene como foco principal las necesidades de nuestros usuarios.',
  },
  items: [
    {
      id: 1,
      icon: img,
      title: 'Creacion de Landing Page',
      subTitle:
        'Si nesecitas colocar tu negocio en el mundo del internet, contáctame para planear, maquetear y desarrollar tu Landing Page.',
      orientation: 'left',
    },
    {
      id: 2,
      icon: img,
      title: 'Diseño de interfaces',
      subTitle:
        'Al utilizar metodologías para planificar el proceso de diseño garantizamos que los productos finales sean lo mas eficiente para los usuarios.',
      orientation: 'left',
    },
  ],
};

export const ExperienceData = {
  items: [
    {
      id: 1,
      icon: numero1,
      title: 'Base de datos relacionales',
      subTitle:
        'Actualmente he trabajado en pequeños proyectos personales con base de datos relacionales como por ejemplo MySQL y PostgreSQL.',
    },
    {
      id: 2,
      icon: numero2,
      title: 'HTML y CSS',
      subTitle:
        'En practica de estos dos importantes lenguajes para construir y visualizar las paginas web aprendiendo del uso de etiquetas y el mundo de los estilos que se pueden aplicar en una web.',
    },
    {
      id: 3,
      icon: numero3,
      title: 'React',
      subTitle:
        'Una libreria muy importante de JavaScript actualmente muy utilizada en magnitudes de proyectos como por ejemplo este portafolio aplicando React.',
    },
    {
      id: 4,
      icon: numero4,
      title: 'Tailwind',
      subTitle:
        'Un framework de CSS muy interesante que me ha llamado la atencion y muy flexible al momento de utilizar en diferentes proyectos.',
    },
  ],
  
};

export const ProjectData = {
  items: [
    {
      id: 1,
      icon: img,
      title: 'Projecto 1',
      subTitle:
        'Descripcion projecto 1.',
    },
    {
      id: 2,
      icon: img,
      title: 'Projecto 2',
      subTitle:
        'Descripcion projecto 2.',
    },
    {
      id: 3,
      icon: img,
      title: 'Projecto 3',
      subTitle:
        'Descripcion projecto 3.',
    },
    {
      id: 4,
      icon: img,
      title: 'Projecto 4',
      subTitle:
        'Descripcion projecto 4.',
    },
  ],
  
};

export const HobbiesData = {
  items: [
    {
      id: 1,
      icon: futbol,
      title: 'Practicar Futbol',
      subTitle:
        'Amo el deporte y mucho mas jugar futbol.',
    },
    {
      id: 2,
      icon: anime,
      title: 'Ver Anime',
      subTitle:
        'En mis tiempos libres observo series animadas.',
    },
    {
      id: 3,
      icon: dibujo,
      title: 'Amo Dibujar',
      subTitle:
        'Me gusta los dibujos a lapiz y digitales.',
    },
    {
      id: 4,
      icon: spagueti,
      title: 'Plato Favorito',
      subTitle:
        'Uno de mis platos favoritos son los spaguetis.',
    },
    {
      id: 5,
      icon: videojugo,
      title: 'Jugar Video Juegos',
      subTitle:
        'Los videojuegos estaran siempre a mi lado.',
    },
    {
      id: 6,
      icon: viaje,
      title: 'Sueño Viajar',
      subTitle:
        'Viajar y conocer diferentes lugares es mi sueño.',
    },
  ],
  
};

export const ContactoData = {
  title: 'Contácto',
  subTitle:
    'Gracias por llegar hasta aquí, si crees que puedo aportar valor a tu equipo o simplemente tienes una pregunta por hacerme, no dudes en escribirme y ¡haré todo lo posible para responderle!',
  btnTextMensaje: 'Enviar Mensaje',
  link: 'https://www.google.com/intl/es-419/gmail/about/',
  correo: 'danielfernandovillota@gmail.com',
  celular: '+57 320 393 0064',
};