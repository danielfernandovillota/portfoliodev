import React from 'react'
import { LayoutExperiencie } from '../components/Layout'
import { ExperienceData, HeroData } from '../data/data'
import ItemExperience from '../components/ItemExperience'
import Carousel from '../components/Carousel'
import react from './../assets/images/react.svg'
import html from './../assets/images/html.svg'
import css from './../assets/images/css.svg'
import tailwind from './../assets/images/tailwind.svg'
import mysql from './../assets/images/mysql.svg'

const slides=[
    react,html,css,tailwind,mysql
]

export const Experience = () => {
  return (
    <section id='experiencia' className='pt-[100px] pb-[150px] font-roboto bg-custom'>
    <LayoutExperiencie>
        <section className='text-center flex flex-col items-center lg:flex-row gap-x-10'>
          <aside className='text-center lg:text-left'>
            <h1 className='text-app-primary-600 font-semibold text-5xl'>Experiencia</h1>
            <section className='flex flex-col gap-y-4 justify-center mt-8 px-6 sm:px-0 sm:gap-y-0 sm:gap-x-4 sm:flex-row lg:justify-start'>
            <div className='w-[300px] h-[400px]'>
                <Carousel autoSlide={true} autoSlideInterval={3000}>
                {slides.map((s, index) => (
                    <img key={index} src={s} className='w-[300px] h-[400px]'/>
                ))}
                </Carousel>
            </div>
            </section>
          </aside>
          <section>
            <ul className='flex flex-col items-center pt-[10px] gap-y-[40px]'>
            {ExperienceData.items.map((item, index) => (
                <ItemExperience key={index} data={item} />
            ))}
            </ul>
          </section>
        </section>
    </LayoutExperiencie>
    </section>
  )
}
