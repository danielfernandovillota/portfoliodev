import React from 'react'
import { ServiceData } from '../data/data'
import { ItemServices } from '../components/ItemServices'


export const Services = () => {
  return (
    <section className='px-8 font-roboto lg:px-0'>
      <aside id='servicios' className='flex flex-col items-center  '>
        <h1 className='font-semibold text-5xl text-app-primary-600 '>{ServiceData.header.title}</h1>
        <p className='mt-4 max-w-[705px] text-lg text-center text-app-secundary-950 font-semibold'>
          {ServiceData.header.subTitle}
        </p>
      </aside>
    <section className='mt-4 py-2 border border-app-primary-600 rounded-xl'>
      
      <section>
        <ul className='px-4 flex flex-col items-center pt-[10px] gap-y-[40px]'>
          {ServiceData.items.map((item, index) => (
            <ItemServices key={index} data={item} />
          ))}
        </ul>
      </section>
    </section>
    </section>
  )
}

export default Services
