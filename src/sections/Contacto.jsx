import React from 'react'
import { ContactoData } from '../data/data'
import { LayoutContacto } from '../components/Layout'

export const Contacto = () => {
  return (
    <section id='contacto' className='px-6 font-roboto'>
        <LayoutContacto>
        <aside className='text-center  lg:text-center py-10 items-center mt-20 border border-app-primary-600 rounded-xl'>
            <section className='lg:px-10'>
              <h1 className=' text-app-primary-600 font-semibold text-5xl break-words whitespace-pre-line mt-10'>
                {ContactoData.title}
              </h1>
              <p className='px-5 text-justify mt-4 text-lg text-app-secundary-950 font-semibold text-center lg:px-0 '>
                {ContactoData.subTitle}
              </p>
              <h3 className='mt-4 text-lg text-app-primary-900 font-extrabold'>{ContactoData.correo}</h3>
              <h3 className='mt-4 text-lg text-app-primary-900 font-extrabold'>{ContactoData.celular}</h3>
            </section>
            <section className='flex flex-col gap-y-4 items-center justify-center mt-6 px-6 sm:px-0 sm:gap-y-0 sm:gap-x-4 sm:flex-row'>
              <a href={ContactoData.link} target='_blank' rel='noopener noreferrer'>
              <button className='flex flex-row items-center justify-center gap-x-2 px-5 py-[10px] text-white font-semibold bg-app-primary-600 border border-app-primary-600 rounded-[32px] '>
                <span>{ContactoData.btnTextMensaje}</span>  
              </button>
              </a>
            </section>
          </aside>
        </LayoutContacto>
    </section>
  )
}

export default Contacto
