import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import { ProjectData } from '../data/data';
import project from './../assets/images/project2.svg'


import 'swiper/css'
import 'swiper/css/pagination'
import {Pagination,Autoplay} from 'swiper'
import { LayoutProject } from '../components/Layout';

export const Project = () => {
   
  return (
    <section id='portafolio' className='px-8 text-app-primary-600 font-roboto'>
        <LayoutProject>
        <div className='text-center py-4'>
            <h3 className='text-app-primary-600 font-semibold text-5xl'>Portafolio</h3>
            <p className='text-app-secundary-950 font-semibold mt-3 text-lg'>¡ Conoce  una pequeña galeria de proyectos recientes  elegidos por mi !</p>
        </div>
        <div className='flex max-x-6xl px-2 mx-auto items-center relative gap-x-6'>
            <div className='lg:w-2/3 w-full'>
                <Swiper slidesPerView={1.2} spaceBetween={20}
                breakpoints={{
                    768:{
                        slidesPerView:2,
                    }
                }} loop={true} autoplay={{
                    delay:3000,
                }}
                pagination={{
                    clickable:true,
                }}
                modules={[Pagination, Autoplay]}
                >
                    {ProjectData.items.map((item, index)=>(
                        <SwiperSlide key={index}>
                            <div className='h-fit w-full p-4 rounded-xl border border-app-primary-600'>
                                <img key={item.id} src={item.icon} alt='' className='rounded-lg'/>
                                <h1 className='text-xl text-center'>{item.title}</h1>
                                <div className='flex flex-col gap-3 text-center'>
                                    <p className='text-app-secundary-950 font-semibold px-2 py-1 inline-block'>{item.subTitle}</p>
                                    <a className='text-white bg-app-primary-600 px-2 py-1 inline-block border border-app-primary-600'>Gitlab</a>
                                </div>
                            </div>
                        </SwiperSlide>
                    ))}
                </Swiper>
            </div>
            <div>
               <img src={project} className='w-[400px] h-[350px]'/> 
            </div>
        </div>
        </LayoutProject>
    </section>
  )
}

export default Project
