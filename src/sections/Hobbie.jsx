import React from 'react'
import { HobbiesData } from '../data/data'
import { LayoutHobbie } from '../components/Layout'

export const Hobbie = () => {
  return (
    <aside className='flex flex-col mt-40 font-roboto'>
    <LayoutHobbie>
    <section id='hobbies'>
        <h1 className='text-app-primary-600 font-semibold text-5xl text-center'>Mis Hobbies</h1>
    </section>
    <section className='flex items-center justify-center min-h-screen container mx-auto mt-8 px-10 '>
        <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8 text-center '>
            {HobbiesData.items.map((items, index)=>(
                <div key={index} className='rounded-xl shadow-lg border border-app-primary-800'>
                <div className='p-5 flex flex-col'>
                    <div className='overflow-hidden'>
                        <img key={items.id} src={items.icon} className='w-[230px] h-[150px]'/>
                    </div>
                    <h5 className='text-2xl font-extrabold md:font-extrabold md:text-lg uppercase text-app-primary-900 mt-3'>{items.title}</h5>
                    <p className='text-app-secundary-950 font-semibold text-lg mt-3'>{items.subTitle}</p>
                </div>
            </div>
            ))}
        </div>
    </section>
    </LayoutHobbie>
    </aside>
  )
}

export default Hobbie
